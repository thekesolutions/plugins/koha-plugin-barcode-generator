$(document).ready(function(){
    $('#cataloguing_additem_newitem input[type="submit"]').click(function() {
        var submit = this;
        var barcode;
        var library_id;
        $('.input_marceditor').each(function() {
            if(/tag_952_subfield_p/.test(this.id)) {
                barcode = this;
            }
            if(/tag_952_subfield_a/.test(this.id)) {
                library_id = this;
            }
        });
        if(!barcode || $(barcode).val()) return true;
        $.ajax('/api/v1/contrib/barcode-generator/barcode?library_id='+$(library_id).val())
        .then(function(res) {
            $(barcode).val(res.barcode);
            submit.click();
        })
        .fail(function(err) {
            console.log(err);
        })

        return false;
    })
})