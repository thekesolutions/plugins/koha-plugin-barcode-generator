# Koha plugin - Barcode generator

This plugin makes cataloguing an item painless. It will make Koha request a
'fresh' barcode on saving the item.

It will only work if the field is not populated already.

## Install

Download the latest _.kpz_ file from the _Project / Releases_ page